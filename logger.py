########################################################################################
# Copyright 2019 by Wolf Krenglowski. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Wolf Krenglowski
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# WOLF KRENGLOWSKI DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# WOLF KRENGLOWSKI BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
########################################################################################

import logging
import os

from pycommons.config import Config


############################################
# Singleton for Logging

class Logger:
    __instance = None

    @classmethod
    def logger(cls):
        if Logger.__instance is None:
            Logger()
        return Logger.__instance.getlogger()

    def __init__(self):
        if Logger.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            # initialize Logging
            loglevel = Config.config().get_value(["log", "level"], "INFO")
            #print "loglevel: " + str(loglevel)

            self.log = logging.getLogger('default')
            self.log.setLevel(loglevel)
            ch = logging.StreamHandler()
            ch.setLevel(loglevel)
            formatter = logging.Formatter('%(threadName)s - %(asctime)s - %(levelname)s - %(message)s')
            ch.setFormatter(formatter)
            self.log.addHandler(ch)
            self.log.info("Logging started")
            self.log.info("Log Level: " + str(loglevel))
            Logger.__instance = self

    def getlogger(self):
        return self.log

    @classmethod
    def say(self, text):
        if (Config.config().get_value(["audio.feedback"], False)):
            os.system('say  -r 200 -v Samantha "%s"' % text)
