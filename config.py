########################################################################################
# Copyright 2019 by Wolf Krenglowski. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Wolf Krenglowski
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# WOLF KRENGLOWSKI DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# WOLF KRENGLOWSKI BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
########################################################################################

import json
import sys

from pycommons.synchronized import synchronized
from pycommons.commons import get_dict_value


__configfile__ = "./config/config.json"

############################################
# Singleton for Config

class Config:
    __instance = None

    @classmethod
    def config(cls):
        if Config.__instance is None:
            Config()
        return Config.__instance

    def __init__(self):
        if Config.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            filename = __configfile__
            #if len(sys.argv) >= 2:
            #    filename = sys.argv[1]

            try:
                print ("using configfile: " + filename)
                with open(filename) as json_data_file:
                    self.__config = json.load(json_data_file)
            except Exception as e:
                sys.exit("Error loading config: " + str(e))
            Config.__instance = self

    @synchronized
    def get_value(self, values, default=None):

        ret = get_dict_value(self.__config, values, default)

        if ret is None:
            sys.exit("configuration error in key %s." % str(values))
        return ret