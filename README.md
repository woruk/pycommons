# pycommons

### To add this submodule to another git project

`git submodule add https://woruk@bitbucket.org/woruk/pycommons.git`

### To initialize a project containing this submodule

`git submodule init`

`git submodule update`

### clone with submodules

`git clone --recurse-submodules`