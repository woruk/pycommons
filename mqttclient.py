########################################################################################
# Copyright 2019 by Wolf Krenglowski. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Wolf Krenglowski
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# WOLF KRENGLOWSKI DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# WOLF KRENGLOWSKI BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
########################################################################################

# Message:
# home/js_wozi/climate/jeelink/1
# <root>/<location>/<category>/<deviceclass>/<sensor>
# root, category, deviceclass: from config
# location, sensor from parameter



from pycommons.config import Config
from pycommons.logger import Logger
from paho.mqtt import client as mqtt

log = Logger.logger()
conf = Config.config()


class MQTTClient():

	def __init__(self):
		
		log.debug("MQTTClient Constructor")
		mqtt_url = conf.get_value(["mqtt", "url"])
		mqtt_port = conf.get_value(["mqtt", "port"])
		mqtt_timeout = conf.get_value(["mqtt", "timeout"])

		self.client = mqtt.Client()
		self.client.connect(mqtt_url, mqtt_port, mqtt_timeout)
		self.client.loop_start()
	
	def notify(self, location, sensor, state):
		log.debug("MQTTClient.notify : " + sensor)

		topic = '%s/%s/%s/%s' % (
			conf.get_value(["mqtt", "topic", "root"]).lower(),
			location.lower(),
			conf.get_value(["mqtt", "topic", "category"]).lower(),
			sensor.lower())
		try:
				log.debug("Topic: " + topic + "  Value: " + str(state))
				self.client.publish(topic, state, 1)
		except Exception as e:
			log.error ("Error: "+str(e))