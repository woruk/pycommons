########################################################################################
# Copyright 2019 by Wolf Krenglowski. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Wolf Krenglowski
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# WOLF KRENGLOWSKI DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# WOLF KRENGLOWSKI BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
########################################################################################

def get_dict_value(dikt, keys, default=None):
    ret = None
    lkeys = list()
    if isinstance(keys, list):
        lkeys.extend(keys)
    elif isinstance(keys, str):
        lkeys.append(keys)

    if len(lkeys) > 0:

        ret = dikt
        while len(lkeys) > 0:
            key = lkeys.pop(0)
            if key in ret:
                ret = ret[key]
            else:
                ret = None
                break
    else:
        print("Empty key")
        ret = None

    if ret is None:
        if default is not None:
            # print("Key %s not found. Using default %s" % (str(key), str(default)))
            ret = default
        else:
            print("Key %s not found. No default set" % str(key))
    return ret
